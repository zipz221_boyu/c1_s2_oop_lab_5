﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassConlsole {

    internal class SimpleClassProgram {

        private const ConsoleColor defTextColor = ConsoleColor.White;
        private const ConsoleColor errorColor = ConsoleColor.Red;
        private const ConsoleColor titleColor = ConsoleColor.Green;
        private const ConsoleColor hintColor = ConsoleColor.Yellow;

        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Title = "Лабораторна робота No5. Завдання 1";
            Console.ForegroundColor = defTextColor;
            start(new Airplane[0]);
        }

        private static void start(Airplane[] items) {
            printMenuTitle("Меню");
            int select = selectMenuItem(new string[] {
                "Вихід",
                "створити рейси",
                "показати список рейсів",
                "показати рейс за номером",
                "показати рейси з відправленням і прибуттям в один день",
                "показати найбільший та найменший час подорожі",
                "відсортувати за спаданням дати відправлення",
                "відсортувати за зростанням часу подорожі",
            });

            Console.Clear();
            switch (select) {
                case 0:
                    return;
                case 1:
                    printMenuTitle("створити рейси");
                    items = ReadAirplaneArray();
                    Console.Clear();
                    break;
                case 2:
                    printMenuTitle("список рейсів");
                    PrintAirplanes(items);
                    break;
                case 3:
                    printMenuTitle("рейс за номером");
                    if (items == null || items.Length < 1) writeLine("Список рейсів порожній!!!", hintColor);
                    else PrintAirplane(items, inputInt($"номер студента від 1 до {items.Length}", 1, items.Length));
                    break;
                case 4:
                    printMenuTitle("список рейсів з відправленням і прибуттям в один день");
                    PrintAirplanes(items, true);
                    break;
                case 5:
                    printMenuTitle("найбільший та найменший час подорожі");
                    if (items == null || items.Length < 1) {
                        writeLine("Список рейсів порожній!!!", hintColor);
                    } else {
                        long max;
                        long min;
                        GetAirplaneInfo(items, out min, out max);
                        write("найбільший час подорожі : ");
                        writeLine($"{max} хвилин", hintColor);
                        write("найменший час подорожі : ");
                        writeLine($"{min} хвилин", hintColor);
                    }
                    break;
                case 6:
                    printMenuTitle("відсортувати за спаданням дати відправлення");
                    SortAirplanesByDate(items);
                    break;
                case 7:
                    printMenuTitle("відсортувати за зростанням часу подорожі");
                    SortAirplanesByTotalTime(items);
                    break;
                default:
                    break;
            }

            start(items);
        }

        private static Airplane[] ReadAirplaneArray() {
            int count = inputInt("скільки рейсів ви хочете створити", 1, int.MaxValue);
            Airplane[] items = new Airplane[count];

            for (int i = 0; i < count; i++) {
                printMenuTitle($"рейс No{i + 1}");
                string startCity = inpuString("місто відправлення");
                string finishCity = inpuString("місто прибуття");
                Date startDate = ReadDate("відправлення");
                Date finishDate = ReadDate("прибуття", startDate);
                items[i] = new Airplane(startCity, finishCity, startDate, finishDate);
            }

            return items;
        }

        private static Date ReadDate(string typeName, Date min = null) {
            printTitle($"дата {typeName}", 40, hintColor);
            Date it = new Date(
                inputInt("рік у форматі \"yyyy\"", min: 0),
                inputInt("місяц у форматі \"MM\"", 1, 12),
                inputInt("день у форматі \"dd\"", 1, 30),
                inputInt("години у форматі \"HH\"", 0, 23),
                inputInt("хвилини у форматі \"mm\"", 0, 59)
            );
            printTitle("", 40, hintColor);

            if (min == null || it.GetTimeInMinutes() > min.GetTimeInMinutes()) return it;

            writeLine($"дата {typeName} повинна бути більшою за {min.ToString()}", errorColor);
            return ReadDate(typeName, min);
        }

        private static void PrintAirplanes(Airplane[] items, bool isShowArrivingToday = false) {
            if (items == null || items.Length < 1) {
                writeLine("Список рейсів порожній!!!", hintColor);
                return;
            }

            for (int i = 0; i < items.Length; i++) {
                if (isShowArrivingToday && !items[i].IsArrivingToday()) continue;
                PrintAirplane(items, i + 1);
            }
        }

        private static void PrintAirplane(Airplane[] items, int number) {
            if (items == null || items.Length < 1 || number < 1 || number > items.Length) return;

            Airplane it = items[number - 1];
            printTitle($"рейс No{number}", 40, hintColor);
            PrintAirplane(it);
            printTitle("", 40, hintColor);
        }

        private static void PrintAirplane(Airplane item) {
            writeLine(item.ToString());
        }

        private static void GetAirplaneInfo(Airplane[] items, out long minTotalTime, out long maxTotalTime) {
            long min = int.MaxValue, max = 0;

            foreach (Airplane it in items) {
                long totalTime = it.GetTotalTime();
                if (totalTime < 0) continue;
                if (totalTime > max) max = totalTime;
                if (totalTime < min) min = totalTime;
            }

            minTotalTime = min;
            maxTotalTime = max;
        }

        private static void SortAirplanesByDate(Airplane[] items) {
            Array.Sort(items, SortAirplanesByDate);
            PrintAirplanes(items);
        }

        private static int SortAirplanesByDate(Airplane first, Airplane second) {
            long a = first.StartDate.GetTimeInMinutes(), b = second.StartDate.GetTimeInMinutes();
            return a > b ? -1 : a < b ? 1 : 0;
        }

        private static void SortAirplanesByTotalTime(Airplane[] items) {
            Array.Sort(items, SortAirplanesByTotalTime);
            PrintAirplanes(items);
        }

        private static int SortAirplanesByTotalTime(Airplane first, Airplane second) {
            long a = first.GetTotalTime(), b = second.GetTotalTime();
            return a > b ? 1 : a < b ? -1 : 0;
        }

        private static int inputInt(string name, int min = int.MinValue, int max = int.MaxValue) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
                return inputInt(name, min, max);
            }

            try {
                int n = int.Parse(value);

                if (n < min) writeLine($"\nЗначення не може бути меншим за {min}. Будь ласка, повторіть спробу!!!", errorColor);
                else if (n > max) writeLine($"\nЗначення не може бути білшим за {max}. Будь ласка, повторіть спробу!!!", errorColor);
                else return n;

                return inputInt(name, min, max);
            } catch (Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть спробу!!!",
                    errorColor
                );
                return inputInt(name, min, max);
            }

        }

        private static string inpuString(string name) {
            writeLine($"Вкажіть {name} та натисніть Enter");
            string value = Console.ReadLine();
            if (!string.IsNullOrEmpty(value)) return value;

            writeLine("Значення не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
            return inpuString(name);
        }

        private static int selectMenuItem(string[] items, int def = -1) {
            if (items == null || items.Length == 0) return def;

            writeLine("\nВкажіть номер пункту та натисніть Enter");

            for (int i = 1; i < items.Length; i++) printMenuItem(i, items[i]);
            printMenuItem(0, items[0]);
            try {
                int it = int.Parse(Console.ReadLine());
                if (it >= 0 && it < items.Length) return it;
                throw new System.FormatException();
            } catch (Exception) {
                writeLine("\nЗробіть свій вибір!!!", errorColor);
            }

            return selectMenuItem(items, def);
        }

        private static void printMenuItem(int number, string name) {
            write($"\t{number}", hintColor);
            writeLine($" -> {name}.");
        }

        private static void printMenuTitle(string name) {
            printTitle(name, 120, titleColor);
        }

        private static void printTitle(string name, int spaseLenght, ConsoleColor color = defTextColor) {
            string it = $"----------{name}";
            for (int i = it.Length; i < spaseLenght; i++) it += "-";
            writeLine();
            writeLine(it, color);
            writeLine();
        }

        private static void writeLine(string value = "", ConsoleColor color = defTextColor) {
            write($"{value}\n", color);
        }

        private static void write(string value = "", ConsoleColor color = defTextColor) {
            Console.ForegroundColor = color;
            Console.Write(value);
            Console.ForegroundColor = defTextColor;
        }

        class Airplane {

            public string StartCity { get; set; } = "";
            public string FinishCity { get; set; } = "";
            public Date StartDate { get; set; } = null;
            public Date FinishDate { get; set; } = null;

            private Airplane() {  }

            public Airplane(string startCity, string finishCity, Date startDate, Date finishDate) {
                StartCity = startCity;
                FinishCity = finishCity;
                StartDate = startDate;
                FinishDate = finishDate;
            }

            public Airplane(Airplane it, string startCity = null, string finishCity = null, Date startDate = null, Date finishDate = null) {
                StartCity = startCity == null ? it.StartCity : startCity;
                FinishCity = finishCity == null ? it.FinishCity : finishCity;
                StartDate = startDate == null ? it.StartDate : startDate;
                FinishDate = finishDate == null ? it.FinishDate : finishDate;
            }

            public override string ToString() {
                string it = $"місто відправлення : {(string.IsNullOrEmpty(StartCity) ? "невідомо" : StartCity)}\n";
                it += $"місто прибуття : {(string.IsNullOrEmpty(FinishCity) ? "невідомо" : FinishCity)}\n";
                it += $"дата відправлення : {(StartDate == null ? "невідомо" : StartDate.ToString())}\n";
                it += $"дата прибуття : {(FinishDate == null ? "невідомо" : FinishDate.ToString())}";
                return it;
            }

            public long GetTotalTime() {
                return (StartDate == null || FinishDate == null) ? -1 : FinishDate.GetTimeInMinutes() - StartDate.GetTimeInMinutes();
            }

            public bool IsArrivingToday() {
                return StartDate != null && FinishDate != null && StartDate.IsToday(FinishDate);
            }
        }

        class Date {

            private int year = -1;
            private int month = 0;
            private int day = 0;
            private int hours = -1;
            private int minutes = -1;

            public int Year {
                get => year;
                set => year = value < 0 ? 0 : value;
            }
            public int Month {
                get => month;
                set => month = value < 1 || value > 12 ? 1 : value;
            }
            public int Day {
                get => day;
                set => day = value < 1 || value > 30 ? 1 : value;
            }
            public int Hours {
                get => hours;
                set => hours = value < 0 || value > 23 ? 0 : value;
            }
            public int Minutes {
                get => minutes;
                set => minutes = value < 0 || value > 59 ? 0 : value;
            }

            private Date() { }

            public Date(int year = 0, int month = 1, int day = 1, int hours = 0, int minute = 0) {
                Year = year;
                Month = month;
                Day = day;
                Hours = hours;
                Minutes = minute;
            }

            public Date(Date it, int year = -1, int month = -1, int day = -1, int hours = -1, int minute = -1) {
                Year = year < 0 ? it.Year : year;
                Month = month < 0 ? it.Month : month;
                Day = day < 0 ? it.Day : day;
                Hours = hours < 0 ? it.Hours : hours;
                Minutes = minute < 0 ? it.Minutes : minute;
            }

            public override string ToString() {
                return $"{day:D2}.{month:D2}.{year} {hours:D2}:{minutes:D2}";
            }

            public long GetTimeInMinutes() {
                long time = GetYearInMinutes(year);
                time += GetMonthInMinutes(month);
                time += GetDayInMinutes(day);
                time += GetHoursInMinutes(hours);
                time += minutes;
                return time;

            }

            public bool IsToday(Date date) {
                return date != null && year == date.Year && month == date.Month && day == date.Day;
            }

            private long GetYearInMinutes(int year) {
                return GetDayInMinutes(year * 365);
            }

            private long GetMonthInMinutes(int month) {
                return GetDayInMinutes(month * 30);
            }

            private long GetDayInMinutes(int day) {
                return GetHoursInMinutes(day * 24);
            }

            private long GetHoursInMinutes(int hour) {
                return hour * 60;
            }
        }
    }
}
